// Module included in the following assemblies:
//
// <List assemblies here, each on a new line>
[id='ingesting-data']
= How to ingest data into your S3 Data Lake

[.lead]
In this guide we will show you how to store data in an S3 Data Lake
that is deployed alongside your Open Data Hub.

.Prerequisites

* A working S3 instance providing an object storage platform that can be used
  as a data lake. The instructions in
  https://opendatahub.io/arch.html#ceph-installation-with-the-rook-operator[our installation guide]
  can be followed to set this up using Ceph.
* A sample data set to be uploaded to S3. In this example
  (and in the guides that follow) we will be using the small MovieLens dataset
  available from Grouplens.org. This archive can be downloaded
  http://files.grouplens.org/datasets/movielens/ml-latest-small.zip[here]. This
  guide assumes that you have downloaded and extracted the MovieLens archive and
  that the contents are available at `$PWD/ml-latest-small`.
* A valid set of credentials for the above S3 object store. In addition, a valid S3
  bucket (or the ability to create one) on the S3 object store will be needed.
* The Python Pip package manager to install the `s3cmd` client. Note that there
  are other S3 clients available, but s3cmd is most recommended by the ODH team.

.Procedure

* Install the `s3cmd` tool, which will be used to write objects to S3. Run the
  following command to install this tool into your user directory:

  pip install --user s3cmd

* Set the necessary environment variables for your S3 access credentials
  by running the following commands (you will need to substitute the values of
  `$ACCESS_KEY` and `$SECRET_KEY` with your actual credentials):

  export AWS_ACCESS_KEY_ID=$ACCESS_KEY
  export AWS_SECRET_ACCESS_KEY=$SECRET_KEY

* Upload the MovieLens dataset objects to your S3 bucket (details of
  each argument in the command follow):

  s3cmd --host $S3_URL put --recursive $PWD/ml-latest-small/ \
    s3://$BUCKET_NAME/odh-guides/movielens-latest-small/

** Here's a breakdown of what's happening in the above command:
*** The `--host $S3_URL` argument specifies the location of your S3
  deployment. You will need to adjust the value of `$S3_URL` for your
  environment.
*** The `put` option specifies that we want to write an object to S3.
*** The `--recursive` option specifies that we are writing a directory to S3.
  All of the contents of the directory will be written to the path specified.
*** `$PWD/ml-latest-small/` specifies the location of the objects that we want to
  write to S3. This should be the location of the MovieLens dataset that you
  downloaded and extracted during the prerequisite steps above.
*** `s3://$BUCKET_NAME/odh-guides/movielens-latest-small/` is the location in S3
  where the objects should be written to. You will need to replace `$BUCKET_NAME`
  with the name of your S3 bucket.

.Additional Resources

* In a future guide we will walk through how to store objects in S3 in a more
  automated, production ready, fashion.
* In the above steps you specified the S3 connection details to the `s3cmd` via
  command line arguments. It is also possible to generate a `s3cmd` config file
  which will save these connection details for future use. To generate this config
  file, simply run the `s3cmd --configure` command.
* More information about the `s3cmd` tool can be found https://s3tools.org/s3cmd[here].
