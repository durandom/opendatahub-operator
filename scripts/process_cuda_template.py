#!env python
# This script processes a CUDA container image build chain located at https://github.com/thoth-station/tensorflow-build-s2i/blob/master/cuda/cuda-build-chain.json
# to be compatible with Open Data Hub operator. It needs to be used when updating the cuda-build-chain.json.j2 template in ODH operator

def removeTrigger(data, trigger_name):
    for o in data['objects']:
        if "spec" in o and "triggers" in o['spec']:
            for t in o['spec']['triggers']:
                if t['type'] == trigger_name:
                    o['spec']['triggers'].remove(t)
                    

import json
import pprint
import sys
with open(sys.argv[1], "r") as fp:
    data = json.load(fp)

#Fix api version and add templatable namespace field
data['apiVersion'] = "template.openshift.io/v1"
data['metadata']['namespace'] = "{{ meta.namespace }}"

# Due to a missing feature (apply or smart merge) in k8s ansible module, which results in BC being overwritten on each reconcilation thus figthing
# with build controller and kicking of build uncontollably, we need to remove the triggers.
removeTrigger(data, "Generic")
removeTrigger(data, "ImageChange")

print(json.dumps(data, indent=4))
